#!/bin/bash
set -e

# Installs KRL Downloader script as cron

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cp $SCRIPT_DIR/krl-downloader.cron /etc/cron.d


# Also installs logrotate config so that cron log doesn't grow excessively
cp $SCRIPT_DIR/krl-downloader.logrotate.conf /etc/logrotate.d/krl-downloader