#!/bin/bash
set -e

# Downloads KRL (Key Revocation List) from desired source. Key Revocation Lists
# are used to deny access to specific Signed SSH Certificates immediately,
# instead of waiting for the certificates to expire on their own.

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source $SCRIPT_DIR/env.sh

OUTPUT_FILE=/etc/ssh/revoked-keys

if [ "$AUTH_USER" = "" ]; then
        echo "ERROR: AUTH_USER variable not set"
        exit 1
elif [ "$AUTH_PASS" = "" ]; then
        echo "ERROR: AUTH_PASS variable not set"
        exit 1
elif [ "$REVOKED_KEYS_FILE_URL" = "" ]; then
        echo "ERROR: REVOKED_KEYS_FILE_URL variable not set"
        exit 1
fi

date +%d-%m-%y/%H:%M:%S
echo "Getting KRL..."

curl -sS --user $AUTH_USER:$AUTH_PASS $REVOKED_KEYS_FILE_URL --output $OUTPUT_FILE

echo "Installed KRL."
echo