# Key Revocation List Downloader

Downloads KRL (Key Revocation List) from desired source. Key Revocation Lists are used to deny access to specific SSH Keys or Signed Certificates immediately, instead of waiting for the certificates to expire on their own.

Signed SSH Certificates are used to grant access to a server, but unlike SSH Keys, they do not have to be manually added to the server. Additionally, they have a built-in expiration date which makes them more secure. In the case of an immediate need for disabling a certificate, however, a list of certificates or keys can be specified and the server will immediately deny them. This script will do this by downloading the KRL and place it in the SSH folder.

**IMPORTANT NOTE:** in order for the KRL to do anything, you MUST specify the `RevokedKeys` config option in the SSHD config with the value being the filepath of the KRL (in the case of this script, it places it in `/etc/ssh/revoked-keys`. It may be a future task to change this to an environment variable for customizing).

You can place this config option directly in the `/etc/ssh/sshd_config` file, or if your version of OpenSSH has a `sshd_config.d` folder, it's recommended that you create a new config file and place it in that directory so it does not interfere with other config options.

## Installation

To use this script, simply clone this repo somewhere on the machine:

    git clone git@gitlab.com:rev-a-shelf/krl-downloader.git

Since this script could potentially be a security risk, it's recommended to clone it somewhere secure, like the root home directory (`/root`) and make sure all the files are owned by root (`sudo chown root:root krl-downloader -R`).

Then, you must specify the URL and credentials used to download the KRL by creating a file with the name `env.sh` using any text editor, and specifying the following environment variables in it:

    AUTH_USER="..."
    AUTH_PASS="..."
    REVOKED_KEYS_FILE_URL="..."

Or by copying and editing the included env.sh.example file.

Then the script can simply be run by doing:

    ./run.sh

In order to run the downloader script on a schedule, you'll need to install it as a cron job, which you can do easily using the included `install-cron.sh` script:

    ./install-cron.sh

This will create a file in `/etc/cron.d` with a predetermined schedule for running the downloader and outputting to a log file within the same directory as the script.